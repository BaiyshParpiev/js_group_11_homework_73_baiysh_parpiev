const express = require('express');
const Vigenere = require('caesar-salad').Vigenere;

const app = express();
const port = 8000;

app.get('', (req, res) => {
    res.set({'Content-type': 'text/plain'}).send('If you want to encode or decode password than you have to write with host + "/encode/something" or "/decode/something" after this you what you write you will get');
});

app.get('/encode/:name', (req, res) => {
    const password = Vigenere.Cipher('password').crypt(req.params.name)
    res.send('Encode: ' + password);
});


app.get('/decode/:name', (req, res) => {
    const password = Vigenere.Decipher('password').crypt(req.params.name);
    res.send('Decode: ' + password);
});


app.listen(port, () => {
    console.log('We are live on ' + port)
});